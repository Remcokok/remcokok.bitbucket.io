var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Game = (function () {
    function Game() {
        var _this = this;
        this._element = document.getElementById('container');
        this._i = [];
        this._countCars = 0;
        this._started = false;
        this._win = false;
        this.keyDownHandler = function (e) {
            if (_this._started == true && _this._win == false) {
                switch (e.keyCode) {
                    case 38:
                        _this._player1.move(50, 0);
                        _this.p1Collision();
                        break;
                    case 39:
                        _this._player1.move(0, 50);
                        _this.p1Collision();
                        break;
                    case 37:
                        _this._player1.move(0, -50);
                        _this.p1Collision();
                        break;
                    case 87:
                        _this._player2.move(50, 0);
                        _this.p2Collision();
                        break;
                    case 68:
                        _this._player2.move(0, 50);
                        _this.p2Collision();
                        break;
                    case 65:
                        _this._player2.move(0, -50);
                        _this.p2Collision();
                        break;
                }
            }
            else {
                switch (e.keyCode) {
                    case 32:
                        _this._started = true;
                        break;
                }
            }
            _this.update();
        };
        this._player1 = new Player1('player1');
        this._player2 = new Player2('player2');
        this._player1.draw();
        this._player2.draw();
        document.addEventListener('keyup', this.keyDownHandler);
        for (var index = 1; index < 23; index++) {
            this._i[index] = new Vehicles(index);
            this._i[index].update();
            this._countCars++;
        }
        this.update();
    }
    Game.prototype.p1Collision = function () {
        var player1Rect = document.getElementById('player1').getBoundingClientRect();
        var player2Rect = document.getElementById('player2').getBoundingClientRect();
        var containerRect = document.getElementById('container').getBoundingClientRect();
        var index;
        var _idNumber = 1;
        for (index = 1; index < this._countCars; index++) {
            var _idName = _idNumber.toString();
            var index_1 = document.getElementById(_idName).getBoundingClientRect();
            if (player1Rect.right == index_1.left) {
                if (player1Rect.bottom == index_1.bottom && player1Rect.top == index_1.top) {
                    this._player1.move(-50, 0);
                }
                else if (player1Rect.bottom < index_1.bottom && player1Rect.bottom > index_1.top) {
                    this._player1.move(-50, 0);
                }
                else if (player1Rect.bottom > index_1.bottom && player1Rect.top < index_1.bottom) {
                    this._player1.move(-50, 0);
                }
            }
            else if (player1Rect.top > index_1.top && player1Rect.bottom > index_1.bottom) {
                if (player1Rect.top < index_1.bottom && player1Rect.bottom > index_1.top) {
                    if (player1Rect.left > index_1.left && player1Rect.right > index_1.right && player1Rect.left < index_1.right) {
                        this._player1.move(0, 50);
                    }
                    else if (player1Rect.left < index_1.left && player1Rect.right < index_1.right && player1Rect.right > index_1.left) {
                        this._player1.move(0, 50);
                    }
                    else if (player1Rect.left == index_1.left && player1Rect.right == index_1.right) {
                        this._player1.move(0, 50);
                    }
                }
            }
            else if (player1Rect.top < index_1.top && player1Rect.bottom < index_1.bottom) {
                if (player1Rect.top < index_1.bottom && player1Rect.bottom > index_1.top) {
                    if (player1Rect.left > index_1.left && player1Rect.right > index_1.right && player1Rect.left < index_1.right) {
                        this._player1.move(0, -50);
                    }
                    else if (player1Rect.left < index_1.left && player1Rect.right < index_1.right && player1Rect.right > index_1.left) {
                        this._player1.move(0, -50);
                    }
                    else if (player1Rect.left == index_1.left && player1Rect.right == index_1.right) {
                        this._player1.move(0, -50);
                    }
                }
            }
            _idNumber++;
        }
        if (player1Rect.right > containerRect.right) {
            this._win = true;
            alert('Player 1 heeft gewonnen!!!!!!');
            location.reload();
        }
        else if (player1Rect.top <= containerRect.top) {
            this._player1.move(0, 50);
        }
        else if (player1Rect.bottom >= containerRect.bottom) {
            this._player1.move(0, -50);
        }
        else if (player1Rect.right == player2Rect.left) {
            if (player1Rect.bottom == player2Rect.bottom && player1Rect.top == player2Rect.top) {
                this._player1.move(-50, 0);
            }
            else if (player1Rect.bottom < player2Rect.bottom && player1Rect.bottom > player2Rect.top) {
                this._player1.move(-50, 0);
            }
            else if (player1Rect.bottom > player2Rect.bottom && player1Rect.top < player2Rect.bottom) {
                this._player1.move(-50, 0);
            }
        }
        else if (player1Rect.top > player2Rect.top && player1Rect.bottom > player2Rect.bottom) {
            if (player1Rect.top < player2Rect.bottom && player1Rect.bottom > player2Rect.top) {
                if (player1Rect.left > player2Rect.left && player1Rect.right > player2Rect.right && player1Rect.left < player2Rect.right) {
                    this._player1.move(0, 50);
                }
                else if (player1Rect.left < player2Rect.left && player1Rect.right < player2Rect.right && player1Rect.right > player2Rect.left) {
                    this._player1.move(0, 50);
                }
                else if (player1Rect.left == player2Rect.left && player1Rect.right == player2Rect.right) {
                    this._player1.move(0, 50);
                }
            }
        }
        else if (player1Rect.top < player2Rect.top && player1Rect.bottom < player2Rect.bottom) {
            if (player1Rect.top < player2Rect.bottom && player1Rect.bottom > player2Rect.top) {
                if (player1Rect.left > player2Rect.left && player1Rect.right > player2Rect.right && player1Rect.left < player2Rect.right) {
                    this._player1.move(0, -50);
                }
                else if (player1Rect.left < player2Rect.left && player1Rect.right < player2Rect.right && player1Rect.right > player2Rect.left) {
                    this._player1.move(0, -50);
                }
                else if (player1Rect.left == player2Rect.left && player1Rect.right == player2Rect.right) {
                    this._player1.move(0, -50);
                }
            }
        }
    };
    Game.prototype.p2Collision = function () {
        var player1Rect = document.getElementById('player1').getBoundingClientRect();
        var player2Rect = document.getElementById('player2').getBoundingClientRect();
        var containerRect = document.getElementById('container').getBoundingClientRect();
        var index;
        var _idNumber = 1;
        for (index = 1; index < this._countCars; index++) {
            var _idName = _idNumber.toString();
            var index_2 = document.getElementById(_idName).getBoundingClientRect();
            if (player2Rect.right == index_2.left) {
                if (player2Rect.bottom == index_2.bottom && player2Rect.top == index_2.top) {
                    this._player2.move(-50, 0);
                }
                else if (player2Rect.bottom < index_2.bottom && player2Rect.bottom > index_2.top) {
                    this._player2.move(-50, 0);
                }
                else if (player2Rect.bottom > index_2.bottom && player2Rect.top < index_2.bottom) {
                    this._player2.move(-50, 0);
                }
            }
            else if (player2Rect.top > index_2.top && player2Rect.bottom > index_2.bottom) {
                if (player2Rect.top < index_2.bottom && player2Rect.bottom > index_2.top) {
                    if (player2Rect.left > index_2.left && player2Rect.right > index_2.right && player2Rect.left < index_2.right) {
                        this._player2.move(0, 50);
                    }
                    else if (player2Rect.left < index_2.left && player2Rect.right < index_2.right && player2Rect.right > index_2.left) {
                        this._player2.move(0, 50);
                    }
                    else if (player2Rect.left == index_2.left && player2Rect.right == index_2.right) {
                        this._player2.move(0, 50);
                    }
                }
            }
            else if (player2Rect.top < index_2.top && player2Rect.bottom < index_2.bottom) {
                if (player2Rect.top < index_2.bottom && player2Rect.bottom > index_2.top) {
                    if (player2Rect.left > index_2.left && player2Rect.right > index_2.right && player2Rect.left < index_2.right) {
                        this._player2.move(0, -50);
                    }
                    else if (player2Rect.left < index_2.left && player2Rect.right < index_2.right && player2Rect.right > index_2.left) {
                        this._player2.move(0, -50);
                    }
                    else if (player2Rect.left == index_2.left && player2Rect.right == index_2.right) {
                        this._player2.move(0, -50);
                    }
                }
            }
            _idNumber++;
        }
        if (player2Rect.right > containerRect.right) {
            this._win = true;
            alert('Player 2 heeft gewonnen!!!!!!');
            location.reload();
        }
        else if (player2Rect.top <= containerRect.top) {
            this._player2.move(0, 50);
        }
        else if (player2Rect.bottom >= containerRect.bottom) {
            this._player2.move(0, -50);
        }
        else if (player2Rect.right == player1Rect.left) {
            if (player2Rect.bottom == player1Rect.bottom && player2Rect.top == player1Rect.top) {
                this._player2.move(-50, 0);
            }
            else if (player2Rect.bottom < player1Rect.bottom && player2Rect.bottom > player1Rect.top) {
                this._player2.move(-50, 0);
            }
            else if (player2Rect.bottom > player1Rect.bottom && player2Rect.top < player1Rect.bottom) {
                this._player2.move(-50, 0);
            }
        }
        else if (player2Rect.top > player1Rect.top && player2Rect.bottom > player1Rect.bottom) {
            if (player2Rect.top < player1Rect.bottom && player2Rect.bottom > player1Rect.top) {
                if (player2Rect.left > player1Rect.left && player2Rect.right > player1Rect.right && player2Rect.left < player1Rect.right) {
                    this._player2.move(0, 50);
                }
                else if (player2Rect.left < player1Rect.left && player2Rect.right < player1Rect.right && player2Rect.right > player1Rect.left) {
                    this._player2.move(0, 50);
                }
                else if (player2Rect.left == player1Rect.left && player2Rect.right == player1Rect.right) {
                    this._player2.move(0, 50);
                }
            }
        }
        else if (player2Rect.top < player1Rect.top && player2Rect.bottom < player1Rect.bottom) {
            if (player2Rect.top < player1Rect.bottom && player2Rect.bottom > player1Rect.top) {
                if (player2Rect.left > player1Rect.left && player2Rect.right > player1Rect.right && player2Rect.left < player1Rect.right) {
                    this._player2.move(0, -50);
                }
                else if (player2Rect.left < player1Rect.left && player2Rect.right < player1Rect.right && player2Rect.right > player1Rect.left) {
                    this._player2.move(0, -50);
                }
                else if (player2Rect.left == player1Rect.left && player2Rect.right == player1Rect.right) {
                    this._player2.move(0, -50);
                }
            }
        }
        else if (player2Rect.right == containerRect.right) {
            this._win = true;
        }
    };
    Game.prototype.update = function () {
        this._player2.update();
        this._player1.update();
    };
    return Game;
}());
var init = function () {
    var game = new Game();
};
window.addEventListener('load', init);
var Players = (function () {
    function Players(name) {
        this._element = document.createElement('img');
        this._name = name;
    }
    Players.prototype.draw = function () {
        var container = document.getElementById('container');
        this._element.src = "./assets/img/" + this._name + ".svg";
        this._element.className = this._name;
        this._element.id = this._name;
        container.appendChild(this._element);
        this._element.style.transform = "translate(" + this._xPos + "px, " + this._yPos + "px)";
    };
    Players.prototype.update = function () {
        this._element.style.transform = "translate(" + this._xPos + "px, " + this._yPos + "px)";
    };
    Players.prototype.move = function (xPosition, yPosition) {
        this._xPos += xPosition;
        this._yPos += yPosition;
        this.update();
    };
    Players.prototype.xPosition = function () {
        return this._xPos;
    };
    Players.prototype.yPosition = function () {
        return this._yPos;
    };
    return Players;
}());
var Player1 = (function (_super) {
    __extends(Player1, _super);
    function Player1(name) {
        var _this = _super.call(this, name) || this;
        _this._xPos = 0;
        _this._yPos = 0;
        return _this;
    }
    return Player1;
}(Players));
var Player2 = (function (_super) {
    __extends(Player2, _super);
    function Player2(name) {
        var _this = _super.call(this, name) || this;
        _this._xPos = -200;
        _this._yPos = 500;
        return _this;
    }
    return Player2;
}(Players));
var Vehicles = (function () {
    function Vehicles(id) {
        this._element = document.createElement('img');
        this._id = id;
        this.draw();
    }
    Vehicles.prototype.draw = function () {
        var randomNumber = Math.random();
        if (randomNumber <= 0.333) {
            this._name = 'white';
        }
        else if (randomNumber <= 0.666) {
            this._name = 'yellow';
        }
        else {
            this._name = 'purple';
        }
        var container = document.getElementById('container');
        this._element.src = "./assets/img/" + this._name + ".svg";
        this._element.className = this._name;
        this._element.id = this._id;
        container.appendChild(this._element);
        this._xPos = Math.round(Math.random() * 2.5) * 1000;
        this._yPos = Math.round(Math.random() * 5) * 100;
        this._element.style.transform = "translate(" + this._xPos + "px, " + this._yPos + "px)";
    };
    Vehicles.prototype.update = function () {
        this._element.style.transform = "translate(" + this._xPos + "px, " + this._yPos + "px)";
    };
    Vehicles.prototype.getId = function () {
        return this._id;
    };
    return Vehicles;
}());
//# sourceMappingURL=main.js.map