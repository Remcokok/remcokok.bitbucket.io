class Game {
    private _element: HTMLElement = document.getElementById('container');
    private _player1: Player1;
    private _player2: Player2;
    private _i: Array<Vehicles> = [];
    private _countCars: number = 0;
    private _started: boolean = false;
    private _win: boolean = false;

    constructor() {
        this._player1 = new Player1('player1');
        this._player2 = new Player2('player2');
        this._player1.draw();
        this._player2.draw();
        document.addEventListener('keyup', this.keyDownHandler);
        for (let index = 1; index < 23; index++) {
            // let number = this._i;
            this._i[index] = new Vehicles(index);
            this._i[index].update();
            this._countCars++;
        }
        this.update();
    }
    public keyDownHandler = (e: KeyboardEvent) => {
        if (this._started == true && this._win == false){
        switch (e.keyCode) {
            
            case 38:
                this._player1.move(50, 0);
                this.p1Collision();
                break;
                
            case 39:
                this._player1.move(0, 50);
                this.p1Collision();
                break;

            case 37:
                this._player1.move(0, -50);
                this.p1Collision();
                break;

            case 87:
                this._player2.move(50, 0);
                this.p2Collision();
                break;

            case 68:
                this._player2.move(0, 50);
                this.p2Collision();
                break;

            case 65:
                this._player2.move(0, -50);
                this.p2Collision();
                break;
            }
        }
            else{
            switch(e.keyCode){
            case 32:
                this._started = true;
                  break;
            }
            
            }
            this.update();
        }
        
        
    

    public p1Collision(): void {
        const player1Rect = document.getElementById('player1').getBoundingClientRect();
        const player2Rect = document.getElementById('player2').getBoundingClientRect();
        const containerRect = document.getElementById('container').getBoundingClientRect();
        let index: any;
        let _idNumber: number = 1;
        for (index = 1; index < this._countCars; index++) {
            let _idName: string = _idNumber.toString();
            const index = document.getElementById(_idName).getBoundingClientRect();

            if (player1Rect.right == index.left) {
                if (player1Rect.bottom == index.bottom && player1Rect.top == index.top) {
                    this._player1.move(-50, 0);
                }
                else if (player1Rect.bottom < index.bottom && player1Rect.bottom > index.top) {
                    this._player1.move(-50, 0);
                }
                else if (player1Rect.bottom > index.bottom && player1Rect.top < index.bottom) {
                    this._player1.move(-50, 0);
                }
            }

            else if (player1Rect.top > index.top && player1Rect.bottom > index.bottom) {
                if (player1Rect.top < index.bottom && player1Rect.bottom > index.top) {
                    if (player1Rect.left > index.left && player1Rect.right > index.right && player1Rect.left < index.right) {
                        this._player1.move(0, 50);
                    }
                    else if (player1Rect.left < index.left && player1Rect.right < index.right && player1Rect.right > index.left) {
                        this._player1.move(0, 50);
                    }
                    else if (player1Rect.left == index.left && player1Rect.right == index.right) {
                        this._player1.move(0, 50);
                    }
                }
            }

            else if (player1Rect.top < index.top && player1Rect.bottom < index.bottom) {
                if (player1Rect.top < index.bottom && player1Rect.bottom > index.top) {
                    if (player1Rect.left > index.left && player1Rect.right > index.right && player1Rect.left < index.right) {
                        this._player1.move(0, -50);
                    }
                    else if (player1Rect.left < index.left && player1Rect.right < index.right && player1Rect.right > index.left) {
                        this._player1.move(0, -50);
                    }
                    else if (player1Rect.left == index.left && player1Rect.right == index.right) {
                        this._player1.move(0, -50);
                    }
                }
            }
            _idNumber++;
        }

        if (player1Rect.right > containerRect.right){
            this._win = true;
            alert('Player 1 heeft gewonnen!!!!!!');
            location.reload();
        }
        else if (player1Rect.top <= containerRect.top) {
            this._player1.move(0, 50);
        }
        else if (player1Rect.bottom >= containerRect.bottom) {
            this._player1.move(0, -50);
        }
        else if (player1Rect.right == player2Rect.left) {
            if (player1Rect.bottom == player2Rect.bottom && player1Rect.top == player2Rect.top) {
                this._player1.move(-50, 0);
            }
            else if (player1Rect.bottom < player2Rect.bottom && player1Rect.bottom > player2Rect.top) {
                this._player1.move(-50, 0);
            }
            else if (player1Rect.bottom > player2Rect.bottom && player1Rect.top < player2Rect.bottom) {
                this._player1.move(-50, 0);
            }
        }

        else if (player1Rect.top > player2Rect.top && player1Rect.bottom > player2Rect.bottom) {
            if (player1Rect.top < player2Rect.bottom && player1Rect.bottom > player2Rect.top) {
                if (player1Rect.left > player2Rect.left && player1Rect.right > player2Rect.right && player1Rect.left < player2Rect.right) {
                    this._player1.move(0, 50);
                }
                else if (player1Rect.left < player2Rect.left && player1Rect.right < player2Rect.right && player1Rect.right > player2Rect.left) {
                    this._player1.move(0, 50);
                }
                else if (player1Rect.left == player2Rect.left && player1Rect.right == player2Rect.right) {
                    this._player1.move(0, 50);
                }
            }
        }

        else if (player1Rect.top < player2Rect.top && player1Rect.bottom < player2Rect.bottom) {
            if (player1Rect.top < player2Rect.bottom && player1Rect.bottom > player2Rect.top) {
                if (player1Rect.left > player2Rect.left && player1Rect.right > player2Rect.right && player1Rect.left < player2Rect.right) {
                    this._player1.move(0, -50);
                }
                else if (player1Rect.left < player2Rect.left && player1Rect.right < player2Rect.right && player1Rect.right > player2Rect.left) {
                    this._player1.move(0, -50);
                }
                else if (player1Rect.left == player2Rect.left && player1Rect.right == player2Rect.right) {
                    this._player1.move(0, -50);
                }
            }
        }
    }


    public p2Collision(): void {
        const player1Rect = document.getElementById('player1').getBoundingClientRect();
        const player2Rect = document.getElementById('player2').getBoundingClientRect();
        const containerRect = document.getElementById('container').getBoundingClientRect();
        let index: any;
        let _idNumber: number = 1;
        for (index = 1; index < this._countCars; index++) {
            let _idName: string = _idNumber.toString();
            const index = document.getElementById(_idName).getBoundingClientRect();

            if (player2Rect.right == index.left) {
                if (player2Rect.bottom == index.bottom && player2Rect.top == index.top) {
                    this._player2.move(-50, 0);
                }
                else if (player2Rect.bottom < index.bottom && player2Rect.bottom > index.top) {
                    this._player2.move(-50, 0);
                }
                else if (player2Rect.bottom > index.bottom && player2Rect.top < index.bottom) {
                    this._player2.move(-50, 0);
                }
            }

            else if (player2Rect.top > index.top && player2Rect.bottom > index.bottom) {
                if (player2Rect.top < index.bottom && player2Rect.bottom > index.top) {
                    if (player2Rect.left > index.left && player2Rect.right > index.right && player2Rect.left < index.right) {
                        this._player2.move(0, 50);
                    }
                    else if (player2Rect.left < index.left && player2Rect.right < index.right && player2Rect.right > index.left) {
                        this._player2.move(0, 50);
                    }
                    else if (player2Rect.left == index.left && player2Rect.right == index.right) {
                        this._player2.move(0, 50);
                    }
                }
            }

            else if (player2Rect.top < index.top && player2Rect.bottom < index.bottom) {
                if (player2Rect.top < index.bottom && player2Rect.bottom > index.top) {
                    if (player2Rect.left > index.left && player2Rect.right > index.right && player2Rect.left < index.right) {
                        this._player2.move(0, -50);
                    }
                    else if (player2Rect.left < index.left && player2Rect.right < index.right && player2Rect.right > index.left) {
                        this._player2.move(0, -50);
                    }
                    else if (player2Rect.left == index.left && player2Rect.right == index.right) {
                        this._player2.move(0, -50);
                    }
                }
            }

            _idNumber++;
        }
        if (player2Rect.right > containerRect.right){
            this._win = true;
            alert('Player 2 heeft gewonnen!!!!!!');
            location.reload();
        }
        else if (player2Rect.top <= containerRect.top) {
            this._player2.move(0, 50);
        }
        else if (player2Rect.bottom >= containerRect.bottom) {
            this._player2.move(0, -50);
        }
        else if (player2Rect.right == player1Rect.left) {
            if (player2Rect.bottom == player1Rect.bottom && player2Rect.top == player1Rect.top) {
                this._player2.move(-50, 0);
            }
            else if (player2Rect.bottom < player1Rect.bottom && player2Rect.bottom > player1Rect.top) {
                this._player2.move(-50, 0);
            }
            else if (player2Rect.bottom > player1Rect.bottom && player2Rect.top < player1Rect.bottom) {
                this._player2.move(-50, 0);
            }
        }

        else if (player2Rect.top > player1Rect.top && player2Rect.bottom > player1Rect.bottom) {
            if (player2Rect.top < player1Rect.bottom && player2Rect.bottom > player1Rect.top) {
                if (player2Rect.left > player1Rect.left && player2Rect.right > player1Rect.right && player2Rect.left < player1Rect.right) {
                    this._player2.move(0, 50);
                }
                else if (player2Rect.left < player1Rect.left && player2Rect.right < player1Rect.right && player2Rect.right > player1Rect.left) {
                    this._player2.move(0, 50);
                }
                else if (player2Rect.left == player1Rect.left && player2Rect.right == player1Rect.right) {
                    this._player2.move(0, 50);
                }
            }
        }

        else if (player2Rect.top < player1Rect.top && player2Rect.bottom < player1Rect.bottom) {
            if (player2Rect.top < player1Rect.bottom && player2Rect.bottom > player1Rect.top) {
                if (player2Rect.left > player1Rect.left && player2Rect.right > player1Rect.right && player2Rect.left < player1Rect.right) {
                    this._player2.move(0, -50);
                }
                else if (player2Rect.left < player1Rect.left && player2Rect.right < player1Rect.right && player2Rect.right > player1Rect.left) {
                    this._player2.move(0, -50);
                }
                else if (player2Rect.left == player1Rect.left && player2Rect.right == player1Rect.right) {
                    this._player2.move(0, -50);
                }
            }
        }
        else if (player2Rect.right == containerRect.right){
            this._win = true;
        }
    }

    public update() {
        this._player2.update();
        this._player1.update();
    }

}