class Vehicles {

    private _element: HTMLImageElement = document.createElement('img');
    private _name: string;
    private _xPos: number;
    private _yPos: number;
    private _id: any;
    constructor(id:any) {
        this._id = id;
        this.draw();
    }

    public draw(){
            const randomNumber = Math.random();
            if (randomNumber <=0.333){
                this._name = 'white';
            }
            else if (randomNumber <=0.666){
                this._name = 'yellow';
            }
            else{
                this._name = 'purple';
            }
            const container = document.getElementById('container');

            this._element.src = `./assets/img/${this._name}.svg`;
            this._element.className = this._name;
            this._element.id = this._id;
            container.appendChild(this._element);
            
            this._xPos = Math.round(Math.random() * 2.5)*1000;
            this._yPos = Math.round(Math.random() * 5) * 100;
   

            this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }
    
    public update() {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }

    public getId(){
        return this._id;
    }
}