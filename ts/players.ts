class Players {
    protected _element: HTMLImageElement = document.createElement('img');
    protected _name: string;
    protected _xPos: number;
    protected _yPos: number;
    
    constructor(name:string) {
        this._name = name;
    } 

    public draw(){
        const container = document.getElementById('container');
        this._element.src = `./assets/img/${this._name}.svg`;
        this._element.className = this._name;
        this._element.id = this._name;
        container.appendChild(this._element);
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }

    public update() {
        this._element.style.transform = `translate(${this._xPos}px, ${this._yPos}px)`;
    }


    public move(xPosition: number, yPosition: number){
        this._xPos += xPosition;
        this._yPos += yPosition;
        this.update();
    }

    public xPosition(): number{
        return this._xPos;
    }

    public yPosition(): number{
        return this._yPos;
    }
}